--------------------
-- Svalbard theme --
--------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_configuration_dir()
local home_path = os.getenv("HOME")

local theme = {}

theme.font          = "fira sans bold 10"
--theme.font          = "cantarell bold 10"

theme.bg_normal     = "#000000"
theme.bg_focus      = "#000000"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(1)
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

theme.fullscreen_hide_border = true
theme.maximized_hide_border = true

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]

theme.prompt_bg_cursor = "#ffffff"

theme.taglist_bg_focus = theme.fg_focus
theme.taglist_bg_occupied = theme.fg_normal
theme.taglist_bg_urgent = theme.bg_urgent
theme.taglist_fg_urgent = theme.bg_urgent
theme.taglist_spacing = dpi(2)
theme.taglist_squares_sel = nil
theme.taglist_squares_unsel = nil
theme.taglist_squares_sel_empty = nil

theme.tasklist_disable_icon = true

theme.systray_icon_spacing = dpi(10)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
theme.notification_max_width = dpi(400)
theme.notification_icon_size = dpi(50)

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)
theme.menu_border_width = dpi(5)
theme.menu_border_color = "#000000"

theme.wallpaper = "#000000"
--theme.wallpaper = home_path.."/Pictures/northernlight_eiscat_svalbard_04.jpg"
--theme.wallpaper = home_path.."/Pictures/Lac du Milieu de Bastan.jpg"
--theme.wallpaper = home_path.."/Pictures/Langkofelgruppe.jpg"

theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

theme.layout_fairh = themes_path.."layouts/fairh.png"
theme.layout_fairv = themes_path.."layouts/fairv.png"
theme.layout_floating  = themes_path.."layouts/floating.png"
theme.layout_magnifier = themes_path.."layouts/magnifier.png"
theme.layout_max = themes_path.."layouts/max.png"
theme.layout_fullscreen = themes_path.."layouts/fullscreen.png"
theme.layout_tilebottom = themes_path.."layouts/tilebottom.png"
theme.layout_tileleft   = themes_path.."layouts/tileleft.png"
theme.layout_tile = themes_path.."layouts/tile.png"
theme.layout_tiletop = themes_path.."layouts/tiletop.png"
theme.layout_spiral  = themes_path.."layouts/spiral.png"
theme.layout_dwindle = themes_path.."layouts/dwindle.png"
theme.layout_cornernw = themes_path.."layouts/cornernw.png"
theme.layout_cornerne = themes_path.."layouts/cornerne.png"
theme.layout_cornersw = themes_path.."layouts/cornersw.png"
theme.layout_cornerse = themes_path.."layouts/cornerse.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
